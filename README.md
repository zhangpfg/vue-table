# vue table

## 示例演示地址
https://q337547038.gitee.io/ak-vue/#/table

## 主要功能

- [x] 可固定表头
- [x] 可固定列
- [x] 可添加序号
- [x] 支持勾选
- [x] 可排序
- [x] 可固定列宽
- [x] 可添加扩展列
- [x] 可拖动改变列宽
- [x] 可合并行或列
- [x] 支持多级表头


### Table Api

|参数|类型|说明|
|-|-|-|
| data          | array         |显示的数据 |
| showHeader    | boolean/true  |是否显示表头 |
| className     | String        |表格类名 |
| hover         | boolean/true  |鼠标经过显示高亮 |
| border        | boolean/true  |是否显示表格纵向边框 |
| stripe        | boolean/true  |是否显示间隔斑马纹 |
| height        | String        |table的高，溢出显示滚动条，且表头固定|
| width         | String        |表格外层div的宽，当单元格总和大于表格width时，出现横向滚动条|
| ellipsis      | boolean/true  |表格单元格文字溢出显示...，在不指定列宽时，各列平分表格宽|
| selectClick   | function      |勾选单列事件|
| sortChange    | function      |排序点击事件|
| selectAllClick| function      |全选或返选事件|
| emptyText     | String        |无数据时显示的文本|
| title         | Boolean/true  |鼠标滑过单元格时显示title提示|
| drag          | boolean/false |允许拖动表头改变当前单元格宽度|
| extendToggle  | boolean/true  |扩展行初始显示或隐藏|
| rowColSpan    | function      |合并行或列方法。通过给传入rowColSpan方法可以实现合并行或列，方法的参数(当前行号rowIndex,当前列号columnIndex,当前行row,当前列column)四个属性。该函数返回一个包含两个数字的数组，第一个rowspan，第二个colspan，即向纵向和横向合并多少个单元格|

### Table Methods
|参数|类型|说明|
|-|-|-|
|getSelectAll         | 返回所有选中的行|
|toggleRowSelection   | 用于多选表格，切换某一行的选中状态，如果使用了第二个参数，则是设置这一行选中与否（selected 为 true 则选中）	row, selected|
|toggleAllSelection   | 用于多选表格，切换所有行的选中状态|
|clearSelection       | 用于多选表格，清空用户的选择|
|clearSort            | 用于清空排序条件|
|resetColumn          | 用于重置表头，当通过js动态改变表格列时用于重置表格列及表头信息|

### Table-column
|参数|类型|说明|
|-|-|-|
|prop           | String        |对应列内容的字段名|
|label          | String        |显示的标题|
|width          | String        |对应列的宽度|
|className      | String        |对应列的类名|
|align          | String        |对齐方式，可选left/center/right|
|type           | String        |对应列类型，可选selection（多选）/index序号/extend扩展列|
|fixed          | Boolean/false |固定列，可选left/right|
|sortBy         | Boolean/false |当前列显示排序按钮|
|title          | Boolean/false |鼠标滑过单元格时显示title提示，仅当table的title为false时有效|

### Table-column Scoped Slot
|参数|说明|
|-|-|
|slot-scope             | 自定义列的内容，参数为 { row, index, extend }|

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
